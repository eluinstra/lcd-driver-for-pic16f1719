/* 
 * File:   LCD.c
 * Author: eluinstra
 *
 * Created on March 11, 2016, 11:52 PM
 * 
 * Copyright 2016 - Under creative commons license 3.0:
 *        Attribution-ShareAlike CC BY-SA
 * 
 * Ported from LiquidCrystal_I2C by F. Malpartida - fmalpartida@gmail.com
 * 
 */
#include "LCD.h"

void pulseEnable(void)
{
  // There is no need for the delays, since the digitalWrite operation
  // takes longer.
	LCD_EN_LAT = HIGH;
	__delay_us(1);          // enable pulse must be > 450ns   
	LCD_EN_LAT = LOW;
}

void write4bits(uint8_t value)
{
	uint8_t i = 0;
  LCD_D4_LAT = (value >> i++) & 0x01;
  LCD_D5_LAT = (value >> i++) & 0x01;
  LCD_D6_LAT = (value >> i++) & 0x01;
  LCD_D7_LAT = (value >> i++) & 0x01;
  pulseEnable();
}

void write8bits(uint8_t value)
{
#ifdef LCD_PORT
	LCD_PORT = value;
#else
	uint8_t i = 0;
  LCD_D0_LAT = (value >> i++) & 0x01;
  LCD_D1_LAT = (value >> i++) & 0x01;
  LCD_D2_LAT = (value >> i++) & 0x01;
  LCD_D3_LAT = (value >> i++) & 0x01;
  LCD_D4_LAT = (value >> i++) & 0x01;
  LCD_D5_LAT = (value >> i++) & 0x01;
  LCD_D6_LAT = (value >> i++) & 0x01;
  LCD_D7_LAT = (value >> i++) & 0x01;
#endif
  pulseEnable();
}

void send(uint8_t value, uint8_t mode)
{
  // Only interested in COMMAND or DATA
	LCD_RS_LAT = (mode == LCD_DATA);
#ifdef LCD_RW_LAT
	// if there is a RW pin indicated, set it low to Write
	LCD_RW_LAT = LOW;
#endif

#if LCD_MODE == LCD_8BITMODE
	write8bits(value);
#else 
	if (mode != LCD_FOUR_BITS)
	{
		write4bits(value >> 4);
		write4bits(value);
	} 
	else
		write4bits(value);
#endif
	__delay_us(LCD_EXEC_TIME); // wait for the command to execute by the LCD
}

void setBacklight(uint8_t value)
{

}

void command(uint8_t value)
{
	send(value,LCD_COMMAND);
}

void lcd_clear()
{
	command(LCD_CLEARDISPLAY);
	__delay_us(LCD_HOME_CLEAR_EXEC);
}

void lcd_home()
{
	command(LCD_RETURNHOME);
	__delay_us(LCD_HOME_CLEAR_EXEC);
}

void lcd_setCursor(uint8_t col, uint8_t row)
{
#if LCD_COLS == 16 && LCD_LINES == 4
	command(LCD_SETDDRAMADDR | (col + row_offsets[row]));
#else
	command(LCD_SETDDRAMADDR | (col + row_offsets[row]));
#endif
}

void lcd_noDisplay()
{
	lcd.control &= ~LCD_DISPLAYON;
	command(LCD_DISPLAYCONTROL | lcd.control);
}

void lcd_display()
{
	lcd.control |= LCD_DISPLAYON;
	command(LCD_DISPLAYCONTROL | lcd.control);
}

void lcd_noCursor()
{
	lcd.control &= ~LCD_CURSORON;
	command(LCD_DISPLAYCONTROL | lcd.control);
}

void lcd_cursor()
{
	lcd.control |= LCD_CURSORON;
	command(LCD_DISPLAYCONTROL | lcd.control);
}

void lcd_noBlink()
{
	lcd.control &= ~LCD_BLINKON;
	command(LCD_DISPLAYCONTROL | lcd.control);
}

void lcd_blink()
{
	lcd.control |= LCD_BLINKON;
	command(LCD_DISPLAYCONTROL | lcd.control);
}

void lcd_scrollDisplayLeft(void)
{
	command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT);
}

void lcd_scrollDisplayRight(void)
{
	command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT);
}

void lcd_leftToRight(void)
{
	lcd.mode |= LCD_ENTRYLEFT;
	command(LCD_ENTRYMODESET | lcd.mode);
}

void lcd_rightToLeft(void)
{
	lcd.mode &= ~LCD_ENTRYLEFT;
	command(LCD_ENTRYMODESET | lcd.mode);
}

void lcd_moveCursorRight(void)
{
	command(LCD_CURSORSHIFT | LCD_CURSORMOVE | LCD_MOVERIGHT);
}

void lcd_moveCursorLeft(void)
{
	command(LCD_CURSORSHIFT | LCD_CURSORMOVE | LCD_MOVELEFT);
}

void lcd_autoscroll(void)
{
	lcd.mode |= LCD_ENTRYSHIFTINCREMENT;
	command(LCD_ENTRYMODESET | lcd.mode);
}

void lcd_noAutoscroll(void)
{
	lcd.mode &= ~LCD_ENTRYSHIFTINCREMENT;
	command(LCD_ENTRYMODESET | lcd.mode);
}

void lcd_backlight(void)
{
	setBacklight(255);
}

void lcd_noBacklight(void)
{
	setBacklight(0);
}

void lcd_on(void)
{
	lcd_display();
	lcd_backlight();
}

void lcd_off(void)
{
	lcd_noBacklight();
	lcd_noDisplay();
}

void lcd_write(uint8_t value)
{
	send(value,LCD_DATA);
}

void lcd_print(const uint8_t *buffer, uint8_t len)
{
  while (len--)
    lcd_write(*buffer++);
}

void lcd_createChar(uint8_t location, uint8_t charmap[])
{
	location &= 0x7; // we only have 8 locations 0-7
	command(LCD_SETCGRAMADDR | (location << 3));
	__delay_us(30);
	for (uint8_t i = 0; i < 8; i++)
	{
		lcd_write(charmap[i]); // call the virtual write method
		__delay_us(40);
	}
}

void lcd_init()
{
	lcd.function = LCD_MODE | LCD_1LINE | LCD_5x8DOTS;
	lcd.polarity = LCD_BP_POSITIVE;
  if (LCD_LINES > 1) 
		lcd.function |= LCD_2LINE;
	lcd.numlines = LCD_LINES;
	lcd.cols = LCD_COLS;

	if ((LCD_DOT_SIZE != LCD_5x8DOTS) && (LCD_LINES == 1))
		lcd.function |= LCD_5x10DOTS;

	// SEE PAGE 45/46 FOR INITIALIZATION SPECIFICATION!
	// according to datasheet, we need at least 40ms after power rises above 2.7V
	// before sending commands. Arduino can turn on way before 4.5V so we'll wait 
	// 50
	// ---------------------------------------------------------------------------
	__delay_ms(100); // 100ms delay

#if LCD_MODE == LCD_8BITMODE
		// this is according to the hitachi HD44780 datasheet
		// page 45 figure 23

		// Send function set command sequence
		command(LCD_FUNCTIONSET | lcd.function);
		__delay_ms(5);  // wait more than 4.1ms

		// second try
		command(LCD_FUNCTIONSET | lcd.function);
		__delay_us(150);

		// third go
		command(LCD_FUNCTIONSET | lcd.function);
		__delay_us(150);
#else
		// this is according to the hitachi HD44780 datasheet
		// figure 24, pg 46

		// we start in 8bit mode, try to set 4 bit mode
		// Special case of "Function Set"
		send(0x03,LCD_FOUR_BITS);
		__delay_us(4500); // wait min 4.1ms

		// second try
		send (0x03,LCD_FOUR_BITS);
		__delay_us(150); // wait min 100us

		// third go!
		send(0x03,LCD_FOUR_BITS);
		__delay_us(150); // wait min of 100us

		// finally, set to 4-bit interface
		send (0x02,LCD_FOUR_BITS);
		__delay_us(150); // wait min of 100us
#endif
	command(LCD_FUNCTIONSET | lcd.function);
	__delay_us(60);

	lcd.control = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;  
	lcd_display();

	lcd_clear();

	lcd.mode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;
	command(LCD_ENTRYMODESET | lcd.mode);

	lcd_backlight();
}