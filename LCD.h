/* 
 * File:   LCD.h
 * Author: eluinstra
 *
 * Created on March 11, 2016, 11:52 PM
 * 
 * Copyright 2016 - Under creative commons license 3.0:
 *        Attribution-ShareAlike CC BY-SA
 * 
 * Ported from LiquidCrystal_I2C by F. Malpartida - fmalpartida@gmail.com
 * 
 */

#ifndef LCD_H
#define	LCD_H

#ifdef	__cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <xc.h>
#include "mcc_generated_files/mcc.h"

// LCD Commands
// ---------------------------------------------------------------------------
#define LCD_CLEARDISPLAY        0x01
#define LCD_RETURNHOME          0x02
#define LCD_ENTRYMODESET        0x04
#define LCD_DISPLAYCONTROL      0x08
#define LCD_CURSORSHIFT         0x10
#define LCD_FUNCTIONSET         0x20
#define LCD_SETCGRAMADDR        0x40
#define LCD_SETDDRAMADDR        0x80

// flags for display entry mode
// ---------------------------------------------------------------------------
#define LCD_ENTRYRIGHT          0x00
#define LCD_ENTRYLEFT           0x02
#define LCD_ENTRYSHIFTINCREMENT 0x01
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off and cursor control
// ---------------------------------------------------------------------------
#define LCD_DISPLAYON           0x04
#define LCD_DISPLAYOFF          0x00
#define LCD_CURSORON            0x02
#define LCD_CURSOROFF           0x00
#define LCD_BLINKON             0x01
#define LCD_BLINKOFF            0x00

// flags for display/cursor shift
// ---------------------------------------------------------------------------
#define LCD_DISPLAYMOVE         0x08
#define LCD_CURSORMOVE          0x00
#define LCD_MOVERIGHT           0x04
#define LCD_MOVELEFT            0x00

// flags for function set
// ---------------------------------------------------------------------------
#define LCD_8BITMODE            0x10
#define LCD_4BITMODE            0x00
#define LCD_2LINE               0x08
#define LCD_1LINE               0x00
#define LCD_5x10DOTS            0x04
#define LCD_5x8DOTS             0x00


// Define COMMAND and DATA LCD Rs (used by send method).
// ---------------------------------------------------------------------------
#define LCD_COMMAND                 0
#define LCD_DATA                    1
#define LCD_FOUR_BITS               2

#define LCD_HOME_CLEAR_EXEC      2000
#define LCD_BACKLIGHT_OFF           0
#define LCD_BACKLIGHT_ON          255

#define LCD_EXEC_TIME 37

// LCD Configuration
// ---------------------------------------------------------------------------
#define LCD_COLS 16
#define LCD_LINES 2
#define LCD_DOT_SIZE LCD_5x8DOTS
#define LCD_MODE LCD_8BITMODE
#define LCD_PORT LATD

//#define LCD_RS_LAT
//#define LCD_RW_LAT
//#define LCD_EN_LAT
//#define LCD_D0_LAT
//#define LCD_D1_LAT
//#define LCD_D2_LAT
//#define LCD_D3_LAT
//#define LCD_D4_LAT
//#define LCD_D5_LAT
//#define LCD_D6_LAT
//#define LCD_D7_LAT

#if LCD_COLS == 16 && LCD_LINES == 4
  const uint8_t row_offsets[] = { 0x00, 0x40, 0x10, 0x50 }; // For 16x4 LCDs
#else
	const uint8_t row_offsets[]   = { 0x00, 0x40, 0x14, 0x54 }; // For regular LCDs
#endif

typedef enum
{
	LCD_BP_POSITIVE,
	LCD_BP_NEGATIVE
} lcd_backlight_polarity_t;

typedef struct
{
	uint8_t function;
	uint8_t control;
	uint8_t mode;
	uint8_t numlines;
	uint8_t cols;
	lcd_backlight_polarity_t polarity;
} lcd_t;

lcd_t lcd;

void lcd_init();
void lcd_clear();
void lcd_home();
void lcd_setCursor(uint8_t col, uint8_t row);
void lcd_noDisplay();
void lcd_display();
void lcd_noCursor();
void lcd_cursor();
void lcd_noBlink();
void lcd_blink();
void lcd_scrollDisplayLeft(void);
void lcd_scrollDisplayRight(void);
void lcd_leftToRight(void);
void lcd_rightToLeft(void);
void lcd_moveCursorRight(void);
void lcd_moveCursorLeft(void);
void lcd_autoscroll(void);
void lcd_noAutoscroll(void);
void lcd_backlight(void);
void lcd_noBacklight(void);
void lcd_on(void);
void lcd_off(void);
void lcd_write(uint8_t value);
void lcd_print(const uint8_t *buffer, uint8_t len);
void lcd_createChar(uint8_t location, uint8_t charmap[]);

#ifdef	__cplusplus
}
#endif

#endif	/* LCD_H */

