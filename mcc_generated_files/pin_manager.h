/**
	@Generated Pin Manager Header File

	@Company:
		Microchip Technology Inc.

	@File Name:
		pin_manager.h

	@Summary:
		This is the Pin Manager file generated using MPLAB� Code Configurator

	@Description:
		This header file provides implementations for pin APIs for all pins selected in the GUI.
		Generation Information :
				Product Revision  :  MPLAB� Code Configurator - v2.25.2
				Device            :  PIC16F1719
				Version           :  1.01
		The generated drivers are tested against the following:
				Compiler          :  XC8 v1.34
				MPLAB             :  MPLAB X v2.35 or v3.00
 */

/*
Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 */

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set LCD_RS aliases
#define LCD_RS_TRIS               TRISC4
#define LCD_RS_LAT                LATC4
#define LCD_RS_PORT               RC4
#define LCD_RS_WPU                WPUC4
#define LCD_RS_ANS                ANSC4
#define LCD_RS_SetHigh()    do { LATC4 = 1; } while(0)
#define LCD_RS_SetLow()   do { LATC4 = 0; } while(0)
#define LCD_RS_Toggle()   do { LATC4 = ~LATC4; } while(0)
#define LCD_RS_GetValue()         RC4
#define LCD_RS_SetDigitalInput()    do { TRISC4 = 1; } while(0)
#define LCD_RS_SetDigitalOutput()   do { TRISC4 = 0; } while(0)

#define LCD_RS_SetPullup()    do { WPUC4 = 1; } while(0)
#define LCD_RS_ResetPullup()   do { WPUC4 = 0; } while(0)
#define LCD_RS_SetAnalogMode()   do { ANSC4 = 1; } while(0)
#define LCD_RS_SetDigitalMode()   do { ANSC4 = 0; } while(0)
// get/set LCD_EN aliases
#define LCD_EN_TRIS               TRISC5
#define LCD_EN_LAT                LATC5
#define LCD_EN_PORT               RC5
#define LCD_EN_WPU                WPUC5
#define LCD_EN_ANS                ANSC5
#define LCD_EN_SetHigh()    do { LATC5 = 1; } while(0)
#define LCD_EN_SetLow()   do { LATC5 = 0; } while(0)
#define LCD_EN_Toggle()   do { LATC5 = ~LATC5; } while(0)
#define LCD_EN_GetValue()         RC5
#define LCD_EN_SetDigitalInput()    do { TRISC5 = 1; } while(0)
#define LCD_EN_SetDigitalOutput()   do { TRISC5 = 0; } while(0)

#define LCD_EN_SetPullup()    do { WPUC5 = 1; } while(0)
#define LCD_EN_ResetPullup()   do { WPUC5 = 0; } while(0)
#define LCD_EN_SetAnalogMode()   do { ANSC5 = 1; } while(0)
#define LCD_EN_SetDigitalMode()   do { ANSC5 = 0; } while(0)
// get/set TX aliases
#define TX_TRIS               TRISC6
#define TX_LAT                LATC6
#define TX_PORT               RC6
#define TX_WPU                WPUC6
#define TX_ANS                ANSC6
#define TX_SetHigh()    do { LATC6 = 1; } while(0)
#define TX_SetLow()   do { LATC6 = 0; } while(0)
#define TX_Toggle()   do { LATC6 = ~LATC6; } while(0)
#define TX_GetValue()         RC6
#define TX_SetDigitalInput()    do { TRISC6 = 1; } while(0)
#define TX_SetDigitalOutput()   do { TRISC6 = 0; } while(0)

#define TX_SetPullup()    do { WPUC6 = 1; } while(0)
#define TX_ResetPullup()   do { WPUC6 = 0; } while(0)
#define TX_SetAnalogMode()   do { ANSC6 = 1; } while(0)
#define TX_SetDigitalMode()   do { ANSC6 = 0; } while(0)
// get/set RX aliases
#define RX_TRIS               TRISC7
#define RX_LAT                LATC7
#define RX_PORT               RC7
#define RX_WPU                WPUC7
#define RX_ANS                ANSC7
#define RX_SetHigh()    do { LATC7 = 1; } while(0)
#define RX_SetLow()   do { LATC7 = 0; } while(0)
#define RX_Toggle()   do { LATC7 = ~LATC7; } while(0)
#define RX_GetValue()         RC7
#define RX_SetDigitalInput()    do { TRISC7 = 1; } while(0)
#define RX_SetDigitalOutput()   do { TRISC7 = 0; } while(0)

#define RX_SetPullup()    do { WPUC7 = 1; } while(0)
#define RX_ResetPullup()   do { WPUC7 = 0; } while(0)
#define RX_SetAnalogMode()   do { ANSC7 = 1; } while(0)
#define RX_SetDigitalMode()   do { ANSC7 = 0; } while(0)
// get/set LCD_D0 aliases
#define LCD_D0_TRIS               TRISD0
#define LCD_D0_LAT                LATD0
#define LCD_D0_PORT               RD0
#define LCD_D0_WPU                WPUD0
#define LCD_D0_ANS                ANSD0
#define LCD_D0_SetHigh()    do { LATD0 = 1; } while(0)
#define LCD_D0_SetLow()   do { LATD0 = 0; } while(0)
#define LCD_D0_Toggle()   do { LATD0 = ~LATD0; } while(0)
#define LCD_D0_GetValue()         RD0
#define LCD_D0_SetDigitalInput()    do { TRISD0 = 1; } while(0)
#define LCD_D0_SetDigitalOutput()   do { TRISD0 = 0; } while(0)

#define LCD_D0_SetPullup()    do { WPUD0 = 1; } while(0)
#define LCD_D0_ResetPullup()   do { WPUD0 = 0; } while(0)
#define LCD_D0_SetAnalogMode()   do { ANSD0 = 1; } while(0)
#define LCD_D0_SetDigitalMode()   do { ANSD0 = 0; } while(0)
// get/set LCD_D1 aliases
#define LCD_D1_TRIS               TRISD1
#define LCD_D1_LAT                LATD1
#define LCD_D1_PORT               RD1
#define LCD_D1_WPU                WPUD1
#define LCD_D1_ANS                ANSD1
#define LCD_D1_SetHigh()    do { LATD1 = 1; } while(0)
#define LCD_D1_SetLow()   do { LATD1 = 0; } while(0)
#define LCD_D1_Toggle()   do { LATD1 = ~LATD1; } while(0)
#define LCD_D1_GetValue()         RD1
#define LCD_D1_SetDigitalInput()    do { TRISD1 = 1; } while(0)
#define LCD_D1_SetDigitalOutput()   do { TRISD1 = 0; } while(0)

#define LCD_D1_SetPullup()    do { WPUD1 = 1; } while(0)
#define LCD_D1_ResetPullup()   do { WPUD1 = 0; } while(0)
#define LCD_D1_SetAnalogMode()   do { ANSD1 = 1; } while(0)
#define LCD_D1_SetDigitalMode()   do { ANSD1 = 0; } while(0)
// get/set LCD_D2 aliases
#define LCD_D2_TRIS               TRISD2
#define LCD_D2_LAT                LATD2
#define LCD_D2_PORT               RD2
#define LCD_D2_WPU                WPUD2
#define LCD_D2_ANS                ANSD2
#define LCD_D2_SetHigh()    do { LATD2 = 1; } while(0)
#define LCD_D2_SetLow()   do { LATD2 = 0; } while(0)
#define LCD_D2_Toggle()   do { LATD2 = ~LATD2; } while(0)
#define LCD_D2_GetValue()         RD2
#define LCD_D2_SetDigitalInput()    do { TRISD2 = 1; } while(0)
#define LCD_D2_SetDigitalOutput()   do { TRISD2 = 0; } while(0)

#define LCD_D2_SetPullup()    do { WPUD2 = 1; } while(0)
#define LCD_D2_ResetPullup()   do { WPUD2 = 0; } while(0)
#define LCD_D2_SetAnalogMode()   do { ANSD2 = 1; } while(0)
#define LCD_D2_SetDigitalMode()   do { ANSD2 = 0; } while(0)
// get/set LCD_D3 aliases
#define LCD_D3_TRIS               TRISD3
#define LCD_D3_LAT                LATD3
#define LCD_D3_PORT               RD3
#define LCD_D3_WPU                WPUD3
#define LCD_D3_ANS                ANSD3
#define LCD_D3_SetHigh()    do { LATD3 = 1; } while(0)
#define LCD_D3_SetLow()   do { LATD3 = 0; } while(0)
#define LCD_D3_Toggle()   do { LATD3 = ~LATD3; } while(0)
#define LCD_D3_GetValue()         RD3
#define LCD_D3_SetDigitalInput()    do { TRISD3 = 1; } while(0)
#define LCD_D3_SetDigitalOutput()   do { TRISD3 = 0; } while(0)

#define LCD_D3_SetPullup()    do { WPUD3 = 1; } while(0)
#define LCD_D3_ResetPullup()   do { WPUD3 = 0; } while(0)
#define LCD_D3_SetAnalogMode()   do { ANSD3 = 1; } while(0)
#define LCD_D3_SetDigitalMode()   do { ANSD3 = 0; } while(0)
// get/set LCD_D4 aliases
#define LCD_D4_TRIS               TRISD4
#define LCD_D4_LAT                LATD4
#define LCD_D4_PORT               RD4
#define LCD_D4_WPU                WPUD4
#define LCD_D4_ANS                ANSD4
#define LCD_D4_SetHigh()    do { LATD4 = 1; } while(0)
#define LCD_D4_SetLow()   do { LATD4 = 0; } while(0)
#define LCD_D4_Toggle()   do { LATD4 = ~LATD4; } while(0)
#define LCD_D4_GetValue()         RD4
#define LCD_D4_SetDigitalInput()    do { TRISD4 = 1; } while(0)
#define LCD_D4_SetDigitalOutput()   do { TRISD4 = 0; } while(0)

#define LCD_D4_SetPullup()    do { WPUD4 = 1; } while(0)
#define LCD_D4_ResetPullup()   do { WPUD4 = 0; } while(0)
#define LCD_D4_SetAnalogMode()   do { ANSD4 = 1; } while(0)
#define LCD_D4_SetDigitalMode()   do { ANSD4 = 0; } while(0)
// get/set LCD_D5 aliases
#define LCD_D5_TRIS               TRISD5
#define LCD_D5_LAT                LATD5
#define LCD_D5_PORT               RD5
#define LCD_D5_WPU                WPUD5
#define LCD_D5_ANS                ANSD5
#define LCD_D5_SetHigh()    do { LATD5 = 1; } while(0)
#define LCD_D5_SetLow()   do { LATD5 = 0; } while(0)
#define LCD_D5_Toggle()   do { LATD5 = ~LATD5; } while(0)
#define LCD_D5_GetValue()         RD5
#define LCD_D5_SetDigitalInput()    do { TRISD5 = 1; } while(0)
#define LCD_D5_SetDigitalOutput()   do { TRISD5 = 0; } while(0)

#define LCD_D5_SetPullup()    do { WPUD5 = 1; } while(0)
#define LCD_D5_ResetPullup()   do { WPUD5 = 0; } while(0)
#define LCD_D5_SetAnalogMode()   do { ANSD5 = 1; } while(0)
#define LCD_D5_SetDigitalMode()   do { ANSD5 = 0; } while(0)
// get/set LCD_D6 aliases
#define LCD_D6_TRIS               TRISD6
#define LCD_D6_LAT                LATD6
#define LCD_D6_PORT               RD6
#define LCD_D6_WPU                WPUD6
#define LCD_D6_ANS                ANSD6
#define LCD_D6_SetHigh()    do { LATD6 = 1; } while(0)
#define LCD_D6_SetLow()   do { LATD6 = 0; } while(0)
#define LCD_D6_Toggle()   do { LATD6 = ~LATD6; } while(0)
#define LCD_D6_GetValue()         RD6
#define LCD_D6_SetDigitalInput()    do { TRISD6 = 1; } while(0)
#define LCD_D6_SetDigitalOutput()   do { TRISD6 = 0; } while(0)

#define LCD_D6_SetPullup()    do { WPUD6 = 1; } while(0)
#define LCD_D6_ResetPullup()   do { WPUD6 = 0; } while(0)
#define LCD_D6_SetAnalogMode()   do { ANSD6 = 1; } while(0)
#define LCD_D6_SetDigitalMode()   do { ANSD6 = 0; } while(0)
// get/set LCD_D7 aliases
#define LCD_D7_TRIS               TRISD7
#define LCD_D7_LAT                LATD7
#define LCD_D7_PORT               RD7
#define LCD_D7_WPU                WPUD7
#define LCD_D7_ANS                ANSD7
#define LCD_D7_SetHigh()    do { LATD7 = 1; } while(0)
#define LCD_D7_SetLow()   do { LATD7 = 0; } while(0)
#define LCD_D7_Toggle()   do { LATD7 = ~LATD7; } while(0)
#define LCD_D7_GetValue()         RD7
#define LCD_D7_SetDigitalInput()    do { TRISD7 = 1; } while(0)
#define LCD_D7_SetDigitalOutput()   do { TRISD7 = 0; } while(0)

#define LCD_D7_SetPullup()    do { WPUD7 = 1; } while(0)
#define LCD_D7_ResetPullup()   do { WPUD7 = 0; } while(0)
#define LCD_D7_SetAnalogMode()   do { ANSD7 = 1; } while(0)
#define LCD_D7_SetDigitalMode()   do { ANSD7 = 0; } while(0)

/**
 * @Param
		none
 * @Returns
		none
 * @Description
		GPIO and peripheral I/O initialization
 * @Example
		PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize(void);

/**
 * @Param
		none
 * @Returns
		none
 * @Description
		Interrupt on Change Handling routine
 * @Example
		PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);

#endif // PIN_MANAGER_H
/**
 End of File
 */